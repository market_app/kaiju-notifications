"""
Abstract and data classes.

Classes
-------

"""

import uuid
from datetime import datetime
from typing import Optional

try:
    from typing import TypedDict
except ImportError:
    # python 3.6 compatibility
    from typing_extensions import TypedDict


__all__ = ('Notification', 'Permissions')


class Permissions:
    """
    Specific notification-related permissions.
    """

    VIEW_OTHERS_NOTIFICATIONS = 'notifications.view_others_notifications'
    MODIFY_OTHERS_NOTIFICATIONS = 'notifications.edit_others_notifications'


class Notification(TypedDict):
    """
    Notification object (dict, row) type hint.
    """

    id: uuid.UUID                   #: notification unique ID
    message: str                    #: notification message text, template or localization key
    format_args: dict               #: format arguments for the message, use them with message.format(**)
    meta: dict                      #: other unuseful metadata
    timestamp: datetime             #: notification creation date
    active: bool                    #: inactive notifications should be ignored
    user_id: uuid.UUID              #: reference to the recipient of the message, None for system
    author_id: Optional[uuid.UUID]  #: reference to the author of the message, None for system
    task_id: Optional[uuid.UUID]    #: reference to the task or process which created this notification
