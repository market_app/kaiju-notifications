"""
Tests for abstract and base classes and interfaces.
"""

import uuid

import pytest

from kaiju_tools.exceptions import NotFound

from ..services import NotificationService


async def test_notification_service(
        database, database_service, notification, admin_session, user_session,
        logger):

    service = NotificationService(app=None, database_service=database_service, logger=logger)
    user_id, admin_id = user_session['user_id'], admin_session['user_id']

    async with database_service:

        notification['user_id'] = admin_id
        data = await service.create(data=notification, session=admin_session)
        admin_note_id = data['id']
        assert data['author_id'] == admin_id

        logger.info('Superuser can view all notifications.')

        notification['user_id'] = user_id
        await service.create(data=notification, session=user_session)
        data = await service.list(session=admin_session)
        assert len(data['data']) == 2

        logger.info('User should only view his own or addressed notifications if he is not a superuser.')

        data = await service.list(session=user_session)
        assert len(data['data']) == 1

        logger.info('User should only update or delete his own or addressed notifications if he is not a superuser.')

        with pytest.raises(NotFound):
            await service.update(id=admin_note_id, data={'active': False}, session=user_session)
