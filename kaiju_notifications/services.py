"""
Notifications related services.

:class:`.NotificationService` provides the basic set of operations and visibility
scopes for non-admin users. Basically, a user can view or edit only his own
or addressed notifications if one doesn't have admin privileges.

Classes
-------

"""

import sqlalchemy as sa

from kaiju_tools.rpc import AbstractRPCCompatible
from kaiju_db.services import SQLService

from .abc import Permissions
from .models import notifications

__all__ = ('NotificationService',)


class NotificationService(SQLService, AbstractRPCCompatible):
    """
    RPC interface for working with notifications.

    Use it to create, update, view or query notifications data from the table.

    :param app: web app instance
    :param database_service: database service instance or instance name
    :param permissions: custom set of RPC permissions
    :param logger: optional logger instance
    """

    Permissions = Permissions
    service_name = 'notifications'
    table = notifications
    insert_columns = {'message', 'format_args', 'meta', 'user_id', 'task_id'}
    update_columns = {'meta', 'active'}

    def __init__(self, app, database_service, permissions=None, logger=None):
        super().__init__(app=app, database_service=database_service, logger=logger)
        AbstractRPCCompatible.__init__(self, permissions=permissions)

    @property
    def routes(self) -> dict:
        return super().routes

    @property
    def permissions(self) -> dict:
        return {
            '*': self.PermissionKeys.GLOBAL_USER_PERMISSION,
            'delete': self.PermissionKeys.GLOBAL_SYSTEM_PERMISSION,
            'm_delete': self.PermissionKeys.GLOBAL_SYSTEM_PERMISSION
        }

    @property
    def validators(self) -> dict:
        return super().validators

    def prepare_insert_data(self, session, data: dict):
        """Injecting an author id from user session."""

        user_id = self.get_user_id(session)
        data = {
            **data,
            'author_id': user_id
        }
        return data

    def _set_user_condition(self, sql, session):
        user_id = self.get_user_id(session)
        sql = sql.where(
            sa.or_(
                self.table.c.author_id == user_id,
                self.table.c.user_id == user_id
            )
        )
        return sql

    def _update_condition_hook(self, sql, session):
        """Places user condition if a user has no admin/system privileges for editing all the data."""

        if not self.has_permission(session, self.Permissions.MODIFY_OTHERS_NOTIFICATIONS):
            sql = self._set_user_condition(sql, session)
        return sql

    def _delete_condition_hook(self, sql, session):
        return self._update_condition_hook(sql, session)

    def _get_condition_hook(self, sql, session):
        """Places user condition if a user has no admin/system privileges for viewing all the data."""

        if not self.has_permission(session, self.Permissions.VIEW_OTHERS_NOTIFICATIONS):
            sql = self._set_user_condition(sql, session)
        return sql
